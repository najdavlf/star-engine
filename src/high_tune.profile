#!/bin/bash

ht_path="$(dirname "$(dirname "$(realpath "$BASH_SOURCE")")")"

export LD_LIBRARY_PATH=$ht_path/lib:${LD_LIBRARY_PATH}
export PATH=$ht_path/bin:${PATH}
export MANPATH=$ht_path/share/man/:${MANPATH}
