# Star-Engine - High-Tune

## Overview

The High-Tune branch of the Star-Engine manages the installation of the
[High-Tune RenDeRer](https://gitlab.com/meso-star/htrdr.git) program from its
source tree: it is actually a [CMake](https://cmake.org) script that on its
execution automatically downloads, builds and deploys the HTRDR program as well
as several of its dependencies.

## Pre-requisites

The HTRDR project is compatible GNU/Linux 64-bits. Its Star-Engine deployment
script relies on the [CMake](https://cmake.org) build system, the
[git](https://git-scm.com) source control, and the [git Large File
Storage](https://git-lfs.github.com) git extension. Several libraries and
programs are directly built from sources; a version of the [GNU Compiler
Collection](https://gcc.gnu.org) compatible with the C99 and C++11 standards is
thus required. Finally, the
[NetCDF](https://www.unidata.ucar.edu/software/netcdf/) C library and headers
as well as an implementation of the [MPI](https://www.mpi-forum.org/)
specification is assumed to be installed on the system. 
Finally, the reference documentation is written with the
[AsciiDoc](http://www.methods.co.nz/asciidoc/) plain-text format and relies on its
tool suite to generate ROFF man pages. If the AsciiDoc tools cannot be
found, the reference documentation will be not installed. Note that HTRDR was
successfully built with GCC 4.8.5, NetCDF 4.1.3 and OpenMPI 1.6.5.

## Install

Use git to clone the High-Tune branch of the Star-Engine repository on the
target machine. Then, generate the CMake project from the `CMakeLists.txt` file
stored in the `cmake` directory (Refer to the [CMake
documentation](https://cmake.org/documentation) for further informations on
CMake). At this step, one can use the CMake user interface to configure the
High-Tune installation (Refer to the [CMake configuration](#config) section for
details on the available options). Finally, build the resulting project. On build
invocation, the script downloads, builds and installs the HTRDR program as
well as its dependencies in the `STAR_INSTALL_PREFIX` directory. By default,
the install destination is the `local` sub-directory of the Star-Engine.

    ~ $ git clone -b high_tune https://gitlab.com/meso-star/star-engine.git high_tune
    ~ $ mkdir high_tune/build && cd high_tune/build
    ~/high_tune/build $ cmake ../cmake
    ~/high_tune/build $ make
    ~/high_tune/build $ cd
    ~ $ source ~/high_tune/local/etc/high_tune.profile
    ~ $ htrdr -h

## CMake configuration
<a name="config"></a>

The default CMake configuration should be appropriate for most
usages. Anyway, this section lists the parameters that can be configured to
tune the deployment of the High-Tune project:

- `CMAKE_BUILD_TYPE`: define how the source based projects are built. The valid
  types are `Debug`, `Release` or `RelWithDebInfo`. The latter type is
  equivalent to the release mode excepted that code assertions and debug
  symbols are enabled as in debug mode. By default, `CMAKE_BUILD_TYPE` is set
  to `Release`. Note that this variable is used only by static build systems as
  GNU/Makefile; when built on Windows with a regular Visual Studio project, one
  have to use the `--target` option of the `cmake` command line to switch
  between the aforementioned modes.
- `CMAKE_INSTALL_RPATH_USE_LINK_PATH`: boolean variable that, when enabled,
  forces the compiled projects to explicitly rely on the libraries used during
  the build process. By default, its value is `False` and, consequently, the
  High-Tune dependencies are resolved at runtime; for instance, on GNU/Linux,
  they are searched in the directories listed in the `LD_LIBRARY_PATH`
  environment variable.
- `EMBREE_VERSION`: define the version of the
  [Embree](https://embree.github.io/) ray-tracing library to use. Currently,
  the only available version is `3.3`.
- `REPO_STAR`: URL toward the public repository of |Meso|Star> that hosts
  several source based dependencies used by High-Tune. Its default value is
  `https://gitlab.com/meso-star/`.
- `REPO_STAR_EX`: URL toward the private repository of |Meso|Star> that hosts
  non-public source based dependencies. Only authorised users can access to
  this repository. Its default value is `git@gitlab.com:meso-star/`.
- `REPO_VAPLV`: URL of a repository that, among others, hosts the sources of
  the [RCMake](https://gitlab.com/vaplv/rcmake.git) and
  [RSys](https://gitlab.com/vaplv/rsys.git) projects used by almost all
  source based dependencies. Its default value is `https://gitlab.com/vaplv/`.
- `REPO_PKG`: URL of the repository that hosts pre-compiled dependencies used
  by High-Tune as, for instance, the Embree library. Its default value is
  `https://www.meso-star.com/packages/v0.3/`.
- `STAR_INSTALL_PREFIX`: define the install directory. By default, the High-Tune
  Renderer and its dependencies are installed in the `local` sub-directory of
  the Star-Engine project.

## Update

To update the High-Tune installation, first check that the Hight-Tune branch of
the Star-Engine repository is up to date. Then force its rebuild to update the
outdated High-Tune project.

    ~/high_tune/build $ git pull origin high_tune
    ~/high_tune/build $ make clean && make

## Licenses

Copyright (C) |Meso|Star> 2015-2019 (<contact@meso-star.com>). Star-Engine is
free software released CeCILL v2 license. You are welcome to redistribute it
under certain conditions; refer to the COPYING files for details.

The licenses of the projects deployed by the Star-Engine script are deployed in
the `share/doc` sub-directory of the Star-Engine install path. Refer to the
documentation of each project for more informations on their associated
license.

